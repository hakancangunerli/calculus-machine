from setuptools import setup


setup(name='Calculus Machine',
      version='0.0.1',
      description='A simple calculator for derivation and integration, and both indefinite and definite integration',
      author='cna',
      url='',
      install_requires=['matplotlib==3.3.3',
                        'mpmath==1.1.0',
                        'numpy==1.19.4',
                        'sympy==1.7.1',
                        ])
